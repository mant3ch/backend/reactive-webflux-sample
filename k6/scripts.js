import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
    // vus: 1000,         // Jumlah pengguna virtual
    // duration: '30s',  // Durasi uji
    stages: [
        { duration: '30s', target: 100 }, // ramp up to 10 users
        { duration: '1m', target: 100 }, // stay at 10 users
        { duration: '10s', target: 0 }, // ramp down to 0 users
    ],
};

export default function() {
    let payload = JSON.stringify({
        accountId: "123123123",
        amount: 1,
        type: "D"
    });

    let params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };

    let res = http.post('http://139.59.126.5/api/accounts/deduct', payload, params);

    // Periksa status respons, Anda juga dapat menambahkan pengujian lain di sini
    if (res.status !== 200) {
        console.error(`Error: Unexpected status code: ${res.status}`);
    }

    // sleep(0.005); // Tunggu selama 5 milidetik sebelum membuat permintaan berikutnya (1000 ms / 200 VUs = 5 ms)
}
