#!/bin/bash

APP_NAME="reactive-webflux-sample"
APP_JAR="/apps/reactive-webflux-sample/reactive-webflux-sample-*.jar"
LOG_FILE="/apps/logs/reactive-webflux-sample/application.log"
SPRING_PROFILES_ACTIVE="dev"

start() {
  echo "Starting $APP_NAME..."
  SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE java -jar $APP_JAR >> $LOG_FILE &
}

stop() {
  echo "Stopping $APP_NAME..."
  if ! kill -9 $(ps ax | grep "$APP_NAME" | grep -v grep | awk '{print $1}'); then
    echo "Failed to stop $APP_NAME, because The App is not running."
  fi
}

restart() {
  stop
  start
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    restart
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
    ;;
esac

exit 0
