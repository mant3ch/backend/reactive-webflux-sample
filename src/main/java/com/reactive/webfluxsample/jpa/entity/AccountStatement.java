package com.reactive.webfluxsample.jpa.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Table(name = "account_statement")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountStatement {

    @Id
    private String id;

    @ManyToOne
    private Account account;

    @Column(name = "beginning_balance")
    private BigDecimal beginningBalance;
    private BigDecimal amount;

    @Column(name = "ending_balance")
    private BigDecimal endingBalance;

    private String type;

    @CreatedDate
    @Column(name = "created_date")
    private ZonedDateTime createdDate;
}
