package com.reactive.webfluxsample.jpa.repository;

import com.reactive.webfluxsample.jpa.entity.AccountStatement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountStatementJpaRepository extends JpaRepository<AccountStatement, String> {
}
