package com.reactive.webfluxsample.jpa.repository;

import com.reactive.webfluxsample.jpa.entity.Account;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface AccountJpaRepository extends JpaRepository<Account, String> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT a FROM Account a WHERE a.id IN :ids")
    List<Account> findAccountsForUpdate(@Param("ids") List<String> ids);

    @Modifying
    @Query("UPDATE Account a SET a.balance = :newBalance WHERE a.id = :id")
    void updateBalance(@Param("id") String id, @Param("newBalance") BigDecimal newBalance);

}
