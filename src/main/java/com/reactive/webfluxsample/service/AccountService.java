package com.reactive.webfluxsample.service;

import com.reactive.webfluxsample.dto.AccountDTO;
import com.reactive.webfluxsample.dto.AccountStatementDTO;
import com.reactive.webfluxsample.dto.BaseResponseDTO;
import com.reactive.webfluxsample.dto.RequestDTO;
import com.reactive.webfluxsample.jpa.entity.AccountStatement;
import com.reactive.webfluxsample.jpa.repository.AccountJpaRepository;
import com.reactive.webfluxsample.jpa.repository.AccountStatementJpaRepository;
import com.reactive.webfluxsample.r2dbc.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService {

    private final AccountJpaRepository accountJpaRepository;
    private final AccountStatementJpaRepository accountStatementJpaRepository;
    private final Scheduler jpaScheduler;
    private final TransactionTemplate transactionTemplate;

    private final AccountRepository accountRepository;

    @SneakyThrows
    @Transactional("transactionManager")
    public Mono<BaseResponseDTO<List<AccountStatementDTO>>> transferFunds(RequestDTO request) {
        return Mono.fromCallable(() -> transactionTemplate.execute(status -> {

            long startTime = System.currentTimeMillis();
            var accounts = accountJpaRepository.findAccountsForUpdate(Arrays.asList(request.getOriginAccountId(), request.getTargetAccountId()));

            var originAccount = accounts.stream().filter(account -> account.getId().equals(request.getOriginAccountId())).findFirst().orElseThrow();
            var targetAccount = accounts.stream().filter(account -> account.getId().equals(request.getTargetAccountId())).findFirst().orElseThrow();
            var originBeginningBalance = originAccount.getBalance();
            var targetBeginningBalance = targetAccount.getBalance();

            // Update account balances
            originAccount.setBalance(originBeginningBalance.subtract(request.getAmount()));
            originAccount.setLastModifiedDate(ZonedDateTime.now());
            targetAccount.setBalance(targetBeginningBalance.add(request.getAmount()));
            targetAccount.setLastModifiedDate(ZonedDateTime.now());

            var originEndingBalance = originAccount.getBalance();
            var targetEndingBalance = targetAccount.getBalance();

            // Save updated accounts
            accountJpaRepository.save(originAccount);
            accountJpaRepository.save(targetAccount);

            var originAccountStatement = AccountStatement.builder()
                    .id(UUID.randomUUID().toString())
                    .account(originAccount)
                    .beginningBalance(originBeginningBalance)
                    .amount(request.getAmount())
                    .endingBalance(originEndingBalance)
                    .type("D")
                    .createdDate(ZonedDateTime.now())
                    .build();

            var targetAccountStatement = AccountStatement.builder()
                    .id(UUID.randomUUID().toString())
                    .account(targetAccount)
                    .beginningBalance(targetBeginningBalance)
                    .amount(request.getAmount())
                    .endingBalance(targetEndingBalance)
                    .type("C")
                    .createdDate(ZonedDateTime.now())
                    .build();

            accountStatementJpaRepository.save(originAccountStatement);
            accountStatementJpaRepository.save(targetAccountStatement);
            long processingTime = System.currentTimeMillis() - startTime;
            log.info("Waktu pemrosesan: {} milidetik",processingTime);
            return new BaseResponseDTO<>(Stream.of(originAccountStatement, targetAccountStatement).map(AccountStatementDTO::new).toList()).withProcessingTimeMillis(processingTime);
        })).subscribeOn(jpaScheduler);
    }

    @SneakyThrows
    @Transactional("transactionManager")
    public Mono<BaseResponseDTO<AccountStatementDTO>> deduct(RequestDTO request) {
        return Mono.fromCallable(() -> transactionTemplate.execute(status -> {

            long startTime = System.currentTimeMillis();
            var accounts = accountJpaRepository.findAccountsForUpdate(Collections.singletonList(request.getAccountId()));

            var account = accounts.getFirst();
            var beginningBalance = account.getBalance();

            // Update account balances
            if("D".equals(request.getType())){
                account.setBalance(beginningBalance.subtract(request.getAmount()));
            }else{
                account.setBalance(beginningBalance.add(request.getAmount()));
            }

            account.setLastModifiedDate(ZonedDateTime.now());

            var endingBalance = account.getBalance();

            // Save updated accounts
            accountJpaRepository.save(account);

            var accountStatement = AccountStatement.builder()
                    .id(UUID.randomUUID().toString())
                    .account(account)
                    .beginningBalance(beginningBalance)
                    .amount(request.getAmount())
                    .endingBalance(endingBalance)
                    .type(request.getType())
                    .createdDate(ZonedDateTime.now())
                    .build();

            accountStatementJpaRepository.save(accountStatement);

            long processingTime = System.currentTimeMillis() - startTime;
            log.info("Waktu pemrosesan: {} milidetik",processingTime);
            return new BaseResponseDTO<>(new AccountStatementDTO(accountStatement)).withProcessingTimeMillis(processingTime);
        })).subscribeOn(jpaScheduler);
    }

    public Mono<BaseResponseDTO<List<AccountDTO>>> getAccount() {
        Instant start = Instant.now(); // Waktu awal pemrosesan

        return accountRepository.findAll()
                .map(AccountDTO::new)
                .collectList()
                .map(accounts -> {
                    Instant end = Instant.now(); // Waktu akhir pemrosesan
                    long processingTimeMillis = Duration.between(start, end).toMillis(); // Menghitung waktu pemrosesan dalam milidetik
                    BaseResponseDTO<List<AccountDTO>> response = new BaseResponseDTO<>(accounts);
                    log.info("Waktu pemrosesan: {} milidetik", processingTimeMillis);
                    return response.withProcessingTimeMillis(processingTimeMillis);
                });
    }

    @Transactional("r2dbcTransactionManager")
    public Mono<BaseResponseDTO<AccountDTO>> editAccount(RequestDTO requestDTO) {
        return accountRepository.findById(requestDTO.getAccountId())
                .switchIfEmpty(Mono.error(new Throwable("Gak ada guys")))
                .flatMap(account -> accountRepository.save(account.withName(requestDTO.getAccountName())))
                .map(AccountDTO::new)
                .map(BaseResponseDTO::new);
    }

}
