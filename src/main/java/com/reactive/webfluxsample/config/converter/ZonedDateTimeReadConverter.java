package com.reactive.webfluxsample.config.converter;

import org.springframework.core.convert.converter.Converter;

import java.time.ZonedDateTime;
import java.util.Date;

public class ZonedDateTimeReadConverter implements Converter<ZonedDateTime, Date> {

    @Override
    public Date convert(ZonedDateTime value) {
        return Date.from(value.toInstant());
    }

}
