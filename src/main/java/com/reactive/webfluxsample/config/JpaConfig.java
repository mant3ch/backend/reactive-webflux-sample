package com.reactive.webfluxsample.config;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.persistence.EntityManagerFactory;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class JpaConfig {

    @Value("${spring.datasource.url}")
    private String hostName;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${spring.datasource.username:default}")
    private String username;
    @Value("${spring.datasource.password:default}")
    private String password;

    @Bean
    public DataSource jpaDataSource() {
        HikariDataSource config = new HikariDataSource();
        config.setDriverClassName(driverClassName);
        config.setJdbcUrl(hostName);
        if(!"default".equals(username)){
            config.setUsername(username);
            config.setPassword(password);
        }

        config.setMaximumPoolSize(200);
        config.setMinimumIdle(5);
        config.setConnectionTimeout(20000);
        config.setIdleTimeout(10000);
        config.setMaxLifetime(1000);

        return config;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource jpaDataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(jpaDataSource);
        em.setPackagesToScan("com.reactive.webfluxsample.jpa"); // Ganti dengan paket tempat entitas Anda
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        return em;
    }

    @Bean
    public SpringLiquibase liquibase(DataSource jpaDataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(jpaDataSource);
        liquibase.setChangeLog("classpath:/db/master.changelog.yaml"); // Lokasi file changelog

        return liquibase;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

    @Bean
    public TransactionTemplate transactionTemplate(PlatformTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }

}