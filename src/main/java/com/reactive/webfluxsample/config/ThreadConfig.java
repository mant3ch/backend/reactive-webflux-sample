package com.reactive.webfluxsample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
public class ThreadConfig {

    @Bean
    public ThreadPoolTaskExecutor jpaTaskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(2000);
        executor.setCorePoolSize(100);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("dear");
        return executor;
    }

    @Bean
    public Scheduler jpaScheduler() {
        return Schedulers.fromExecutor(jpaExecutor());
    }

    @Bean
    public Scheduler jpaSchedulerVirtual() {
        return Schedulers.fromExecutor(Executors.newVirtualThreadPerTaskExecutor());
    }

    @Bean
    public Executor jpaExecutor() {
        return Executors.newVirtualThreadPerTaskExecutor();
    }

}