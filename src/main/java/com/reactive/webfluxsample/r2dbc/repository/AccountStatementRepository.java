package com.reactive.webfluxsample.r2dbc.repository;

import com.reactive.webfluxsample.r2dbc.entity.AccountStatement;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

public interface AccountStatementRepository extends R2dbcRepository<AccountStatement, String> {
}
