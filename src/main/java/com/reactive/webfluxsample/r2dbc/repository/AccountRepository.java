package com.reactive.webfluxsample.r2dbc.repository;

import com.reactive.webfluxsample.r2dbc.entity.Account;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

public interface AccountRepository extends R2dbcRepository<Account, String> {
}
