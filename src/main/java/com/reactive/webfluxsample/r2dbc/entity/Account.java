package com.reactive.webfluxsample.r2dbc.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Table(name = "account")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@With
public class Account{

    @Id
    private String id;
    private String name;
    private BigDecimal balance;

    @CreatedDate
    @Column("created_date")
    private ZonedDateTime createdDate;

    @LastModifiedDate
    @Column("last_modified_date")
    private ZonedDateTime lastModifiedDate;

}
