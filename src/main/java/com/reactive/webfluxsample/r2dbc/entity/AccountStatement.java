package com.reactive.webfluxsample.r2dbc.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Table("account_statement")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountStatement {

    @Id
    private String id;

    @Column("account_id")
    private String accountId;

    @Column("beginning_balance")
    private BigDecimal beginningBalance;
    private BigDecimal amount;

    @Column("ending_balance")
    private BigDecimal endingBalance;

    private String type;

    @CreatedDate
    @Column("created_date")
    private ZonedDateTime createdDate;
}
