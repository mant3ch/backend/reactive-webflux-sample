package com.reactive.webfluxsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveWebfluxSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveWebfluxSampleApplication.class, args);
    }

}
