package com.reactive.webfluxsample.dto;

import com.reactive.webfluxsample.jpa.entity.AccountStatement;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Getter
@Setter
public class AccountStatementDTO {

    private AccountDTO account;
    private BigDecimal beginningBalance;
    private BigDecimal amount;
    private BigDecimal endingBalance;
    private String type;
    private ZonedDateTime createdDate;

    public AccountStatementDTO(AccountStatement accountStatement){
        if(accountStatement.getAccount() != null){
            this.account = AccountDTO.builder()
                    .id(accountStatement.getAccount().getId())
                    .name(accountStatement.getAccount().getName())
                    .balance(accountStatement.getAccount().getBalance())
                    .build();
        }

        this.beginningBalance = accountStatement.getBeginningBalance();
        this.amount = accountStatement.getAmount();
        this.endingBalance = accountStatement.getEndingBalance();
        this.type = accountStatement.getType();
        this.createdDate = accountStatement.getCreatedDate();
    }
}
