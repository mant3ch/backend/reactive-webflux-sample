package com.reactive.webfluxsample.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class RequestDTO {

    private String originAccountId;
    private String targetAccountId;
    private BigDecimal amount;
    private String accountId;
    private String type;
    private String accountName;

}
