package com.reactive.webfluxsample.dto;

import com.reactive.webfluxsample.r2dbc.entity.Account;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDTO {

    private String id;
    private String name;
    private BigDecimal balance;

    public AccountDTO(Account account){
        this.id = account.getId();
        this.name = account.getName();
        this.balance = account.getBalance();
    }

    public AccountDTO(com.reactive.webfluxsample.jpa.entity.Account account){
        this.id = account.getId();
        this.name = account.getName();
        this.balance = account.getBalance();
    }

}
