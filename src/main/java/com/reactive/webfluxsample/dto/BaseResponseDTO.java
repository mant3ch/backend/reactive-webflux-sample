package com.reactive.webfluxsample.dto;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@With
public class BaseResponseDTO<T> {

    private String status;
    private String message;
    private T data;
    private long processingTimeMillis;

    public BaseResponseDTO(T data){
        this.status = "0";
        this.message = "SUCCESS";
        this.data = data;
    }

}
