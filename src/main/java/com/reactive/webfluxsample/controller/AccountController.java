package com.reactive.webfluxsample.controller;

import com.reactive.webfluxsample.dto.AccountDTO;
import com.reactive.webfluxsample.dto.AccountStatementDTO;
import com.reactive.webfluxsample.dto.BaseResponseDTO;
import com.reactive.webfluxsample.dto.RequestDTO;
import com.reactive.webfluxsample.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/transfer")
    public Mono<BaseResponseDTO<List<AccountStatementDTO>>> transferFunds(@RequestBody RequestDTO request) {
        return accountService.transferFunds(request);
    }

    @PostMapping("/deduct")
    public Mono<BaseResponseDTO<AccountStatementDTO>> deduct(@RequestBody RequestDTO request) {
        return accountService.deduct(request);
    }

    @GetMapping
    public Mono<BaseResponseDTO<List<AccountDTO>>> getAccount() {
        return accountService.getAccount();
    }

    @PutMapping
    public Mono<BaseResponseDTO<AccountDTO>> editAccount(@RequestBody RequestDTO requestDTO) {
        return accountService.editAccount(requestDTO);
    }
}
