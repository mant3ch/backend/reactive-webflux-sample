INSERT INTO product (id, code, name, base_price, selling_price, created_date, created_by, last_modified_date, last_modified_by)
values ('prodId1', 'prodCode1', 'Macbook Pro M1', 20000000, 25000000, now(), 'Firman', now(), 'Firman');

INSERT INTO product (id, code, name, base_price, selling_price, created_date, created_by, last_modified_date, last_modified_by)
values ('prodId2', 'prodCode2', 'Macbook Pro M1 Pro', 30000000, 40000000, now(), 'Hanafi', now(), 'Hanafi');
