# Use an official Maven runtime as a parent image
FROM azul/zulu-openjdk-alpine:21-jre


# Set the working directory
WORKDIR /usr/src/app

# Copy the JAR file from the CI/CD build context to the image
COPY target/*.jar app.jar

# Expose the port the app runs on (optional)
EXPOSE 8080

# Specify the command to run on container start
CMD ["java", "-jar", "app.jar"]
